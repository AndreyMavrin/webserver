package models

import "encoding/json"

type Hacker struct {
	Name  string  `json:"name"`
	Score float64 `json:"score"`
}

func (h Hacker) MarshalBinary() ([]byte, error) {
	return json.Marshal(h)
}
