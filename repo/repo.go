package repo

import (
	"context"
	"encoding/json"
	"fmt"
	"webserver/models"

	"github.com/go-redis/redis/v9"
)

type Repo struct {
	Redis *redis.Client
}

var insertedHackers = []models.Hacker{
	{
		Name:  "Richard Stallman",
		Score: 1953,
	},
	{
		Name:  "Alan Kay",
		Score: 1940,
	},
	{
		Name:  "Yukihiro Matsumoto",
		Score: 1965,
	},
	{
		Name:  "Claude Shannon",
		Score: 1916,
	},
	{
		Name:  "Linus Torvalds",
		Score: 1969,
	},
	{
		Name:  "Alan Turing",
		Score: 1912,
	},
}

func NewRepo(redis *redis.Client) *Repo {
	return &Repo{
		Redis: redis,
	}
}

func (r *Repo) ListHackers(ctx context.Context) ([]*models.Hacker, error) {
	hackers := make([]*models.Hacker, 0)

	result, err := r.Redis.ZRangeWithScores(ctx, "hacker", 0, int64(len(insertedHackers))).Result()
	if err != nil {
		return nil, err
	}
	for _, zItem := range result {
		zHacker := new(models.Hacker)

		if err := json.Unmarshal([]byte(fmt.Sprint(zItem.Member)), &zHacker); err != nil {
			return nil, err
		}

		hacker := &models.Hacker{
			Name:  zHacker.Name,
			Score: zItem.Score,
		}
		hackers = append(hackers, hacker)

	}

	return hackers, nil
}

func (r *Repo) FillRedis(ctx context.Context) error {
	for _, hacker := range insertedHackers {
		if _, err := r.Redis.ZAdd(ctx, "hacker", redis.Z{
			Score:  hacker.Score,
			Member: hacker,
		}).Result(); err != nil {
			return err
		}

	}
	return nil
}
