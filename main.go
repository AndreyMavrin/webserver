package main

import (
	"context"
	"log"
	"net/http"
	"webserver/api"
	"webserver/repo"

	"github.com/go-redis/redis/v9"
	"github.com/gofiber/fiber/v2"
)

func main() {

	app := fiber.New()

	app.Get("/health", func(c *fiber.Ctx) error {
		c.Status(http.StatusOK)
		return nil
	})

	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	repo := repo.NewRepo(rdb)
	api := api.NewAPI(repo)

	if err := repo.FillRedis(context.Background()); err != nil {
		log.Fatal(err)
	}

	app.Get("/json/hackers", api.Hackers)

	addr := ":8010"
	log.Printf("Starting server at %s", addr)
	log.Fatal(app.Listen(addr))
}
