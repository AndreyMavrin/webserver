package api

import (
	"net/http"
	"webserver/repo"

	"github.com/gofiber/fiber/v2"
)

type API struct {
	Repo *repo.Repo
}

func NewAPI(repo *repo.Repo) *API {
	return &API{
		Repo: repo,
	}
}

func (a *API) Hackers(c *fiber.Ctx) error {
	hackers, err := a.Repo.ListHackers(c.Context())
	if err != nil {
		c.Status(http.StatusNotFound)
		return err
	}

	c.Set("Content-Type", "application/json")

	if len(hackers) == 0 {
		return c.Status(http.StatusNotFound).JSON(&fiber.Map{
			"success": false,
			"error":   "There are no hackers!",
		})
	}
	return c.JSON(&fiber.Map{
		"success": true,
		"posts":   hackers,
	})
}
